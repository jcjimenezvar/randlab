const express = require('express');
let router = express.Router();

const getData = require('../../../services/v1/getData')
const getAllData = require('../../../services/v1/getAllData')

// Get Products Coverages List
router.get('/get-data/:id', getData);
router.get('/get-all-data', getAllData);

module.exports = router;