const express = require('express');
let router = express.Router();

const saveData = require('../../../services/v1/saveData')

// Get Products Coverages List
router.post('/save-data', saveData);

module.exports = router;