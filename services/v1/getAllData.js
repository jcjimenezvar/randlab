const Joi = require('@hapi/joi');

const { getAllDataDao } = require('../../DAO/store/getData.dao');
const { STATUS_CODE_ENUM } = require('../../utils/index');

const getAllData = async (req, res) => {
  try {

    const data = await getAllDataDao()
      .then((data) => {
        if (!data) {
          return {
            error: `No records found`,
            status: STATUS_CODE_ENUM.NOT_FOUND,
          };
        }
        return data;
      })
      .catch((err) => {
        console.log(`An error has ocurred getting data: ${err}`);
        return {
          error: `An error has ocurred getting data: ${err}`,
          status: STATUS_CODE_ENUM.INTERNAL_SERVER_ERROR,
        };
      });

    if (data.error) {
      return res.status(data.status).json({
        success: false,
        error: data.error,
      });
    }

    return res.status(STATUS_CODE_ENUM.OK).json({
      success: true,
      data: data,
      message: 'Successfully',
    });
  } catch (error) {
    return res.status(STATUS_CODE_ENUM.INTERNAL_SERVER_ERROR).json({
      success: false,
      message: `An error has ocurred: ${error.message}`,
    });
  }
};

module.exports = getAllData;
