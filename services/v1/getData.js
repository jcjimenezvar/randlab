const Joi = require('@hapi/joi');

const { getDataDao } = require('../../DAO/store/getData.dao');
const { STATUS_CODE_ENUM } = require('../../utils/index');

const getData = async (req, res) => {
  try {
    let { id } = req.params;

    const schema = Joi.object({
      id: Joi.number().label(`The 'id' parameter`).required(),
    });

    const validationResult = schema.validate({ id });

    if (validationResult.error) {
      return res.status(STATUS_CODE_ENUM.BAD_REQUEST).json({
        success: false,
        error: `Invalid data on request: ${validationResult.error.message}`,
      });
    }

    const data = await getDataDao(validationResult.value.id)
      .then((data) => {
        if (!data) {
          return {
            error: `No records found for the 'id': ${validationResult.value.id}`,
            status: STATUS_CODE_ENUM.NOT_FOUND,
          };
        }
        return data;
      })
      .catch((err) => {
        console.log(`An error has ocurred getting data: ${err}`);
        return {
          error: `An error has ocurred getting data: ${err}`,
          status: STATUS_CODE_ENUM.INTERNAL_SERVER_ERROR,
        };
      });

    if (data.error) {
      return res.status(data.status).json({
        success: false,
        error: data.error,
      });
    }

    return res.status(STATUS_CODE_ENUM.OK).json({
      success: true,
      data: data,
      message: 'Successfully',
    });
  } catch (error) {
    return res.status(STATUS_CODE_ENUM.INTERNAL_SERVER_ERROR).json({
      success: false,
      message: `An error has ocurred: ${error.message}`,
    });
  }
};

module.exports = getData;
