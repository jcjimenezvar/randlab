const Joi = require('@hapi/joi');
const mongoose = require('mongoose');

const { saveDataDao } = require('../../DAO/store/saveData.dao');
const { STATUS_CODE_ENUM } = require('../../utils/index');

const saveData = async (req, res) => {
  try {
    let { id, name } = req.body;

    const schema = Joi.object({
      id: Joi.number().label('The "id" parameter').required(),
      name: Joi.string().label('The "name" parameter').required(),
    });

    const validationResult = schema.validate({ id, name });

    if (validationResult.error) {
      return res.status(STATUS_CODE_ENUM.BAD_REQUEST).json({
        success: false,
        error: `Invalid data on request: ${validationResult.error.message}`,
      });
    }

    const savedData = await saveDataDao({
      _id: new mongoose.Types.ObjectId(),
      id: validationResult.value.id,
      name: validationResult.value.name,
    }).catch((err) => {
      console.log(`An error has ocurred saving data: ${err}`);
      return {
        error: `An error has ocurred saving data: ${err}`,
        status: STATUS_CODE_ENUM.CONFLICT,
      };
    });

    if (savedData.error) {
      return res.status(savedData.status).json({
        success: false,
        error: savedData.error,
      });
    }

    return res.status(STATUS_CODE_ENUM.OK).json({
      success: true,
      data: savedData,
      message: 'Data saved successfully',
    });
  } catch (error) {
    return res.status(STATUS_CODE_ENUM.INTERNAL_SERVER_ERROR).json({
      success: false,
      message: `An error has ocurred: ${error}`,
    });
  }
};

module.exports = saveData;
