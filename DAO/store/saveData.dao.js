const StoreModel = require('../../models/store.model');

module.exports = {
  saveDataDao: (data) => {
    return new Promise((resolve, reject) => {
      StoreModel.create(data)
        .then((store) => {
          resolve(store);
        })
        .catch((err) => {
          console.log('error occurred', err);
          reject(err);
        });
    });
  },
};
