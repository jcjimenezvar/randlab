const StoreModel = require('../../models/store.model');

module.exports = {
  getDataDao: (id) => {
    return new Promise((resolve, reject) => {
      StoreModel.findOne({
        id: id,
      })
        .then((store) => {
          resolve(store);
        })
        .catch((err) => {
          console.log('error occurred', err);
          reject(err);
        });
    });
  },
  getAllDataDao: () => {
    return new Promise((resolve, reject) => {
      StoreModel.find()
        .then((store) => {
          resolve(store);
        })
        .catch((err) => {
          console.log('error occurred', err);
          reject(err);
        });
    });
  },
};
