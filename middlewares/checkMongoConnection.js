const mongoose = require('mongoose');
const { MONGO_URI } = require('../config/keys');

const connectMongo = async () => {
  console.log(MONGO_URI);
  await mongoose
    .connect(MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    })
    .then(console.log('Connection with mongo atlas established succesfully'))
    .catch((error) => console.log(error.message));
  mongoose.set('debug', true);
};

const checkMongoConnection = async (req, res, next) => {
  // console.log(mongoose.connection.readyState);
  if (mongoose.connection.readyState == 0) {
    await connectMongo();
  }
  next();
};

module.exports = { connectMongo, checkMongoConnection };
