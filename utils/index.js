const STATUS_CODE_ENUM = {
    OK: 200,
    BAD_REQUEST: 400,
    NOT_FOUND: 404,
    CONFLICT: 409,
    INTERNAL_SERVER_ERROR: 500

}

module.exports = {
    STATUS_CODE_ENUM
}


