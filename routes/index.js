const FS = require('fs');
const apiDocsJson = FS.readFileSync('api-docs.json', 'utf8');

const swaggerUiExpress = require('swagger-ui-express');

const apiRoute = require('./api');

const init = (app) => {
  app.get('*', function (req, res, next) {
    console.log('Request was made to: ' + req.originalUrl);
    return next();
  });
  app.use(
    '/api-docs',
    swaggerUiExpress.serve,
    swaggerUiExpress.setup(apiDocsJson),
    apiRoute
  );
  app.use('/api', apiRoute);
};

module.exports = {
  init: init,
};
