// Controller That handle incoming requests
const storeController = require('../../controllers/api/v1/storeController');
const queryController = require('../../controllers/api/v1/queryController');

const express = require('express');
let router = express.Router();

router.use('/store', storeController);
router.use('/query', queryController);

module.exports = router;
