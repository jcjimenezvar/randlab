const express = require("express");
const v1ApiController = require("./v1");
let router = express.Router();
const {
    checkMongoConnection
  } = require('../../middlewares/checkMongoConnection');
  
  router.use('/v1', checkMongoConnection, v1ApiController);
module.exports = router;

