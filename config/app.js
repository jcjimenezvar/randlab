const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');

const FS = require('fs');
const apiDocsJson = FS.readFileSync('api-docs.json', 'utf8');
const swaggerUiExpress = require('swagger-ui-express');

const { connectMongo } = require('../middlewares/checkMongoConnection');
const { STATUS_CODE_ENUM } = require('../utils/index');

module.exports = () => {
  let app = express(),
    create,
    start;

  create = async () => {
    // Routes which should handle requests
    let routes = require('../routes');

    app.use(
      '/api-docs',
      swaggerUiExpress.serve,
      swaggerUiExpress.setup(JSON.parse(apiDocsJson))
    );
    
    // set all the server things
    app.set('env', process.env.NODE_ENV);
    app.set(
      'port',
      process.env.NODE_PORT !== undefined ? process.env.NODE_PORT : 8000
    );
    app.set(
      'hostname',
      process.env.HOSTNAME !== undefined ? process.env.HOSTNAME : 'localhost'
    );
    app.use(cors());
    app.use(morgan('dev'));

    // add middleware to parse the json
    app.use(bodyParser.json());
    app.use(
      bodyParser.urlencoded({
        extended: false,
      })
    );

    // Connect with MongoDB
    connectMongo();

    routes.init(app);

    //CORS errors control
    app.use((req, res, next) => {
      res.header('Access-Control-Allow-Origin', '*');
      res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Authorization'
      );
      if (req.method == 'OPTIONS') {
        res.header(
          'Access-Control-Allow-Methods',
          'PUT, POST, PATCH, DELETE, GET'
        );
        return res.status(STATUS_CODE_ENUM.OK).json({});
      }
      next();
    });

    app.use((req, res, next) => {
      const error = new Error('Route Not Found');
      error.status = STATUS_CODE_ENUM.NOT_FOUND;
      next(error);
    });

    app.use((error, req, res, next) => {
      res.status(error.status || STATUS_CODE_ENUM.INTERNAL_SERVER_ERROR);
      res.json({
        error: error.message,
      });
    });
  };

  start = () => {
    let hostname = app.get('hostname'),
      port = app.get('port');
    app.listen(port, () => {
      console.log(
        `Express Server on server listening on - http://${hostname}:${port}`
      );
    });
  };

  return {
    create: create,
    start: start,
  };
};
