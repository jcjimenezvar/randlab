const mongoose = require('mongoose');

const storeSchema = mongoose.Schema(
  {
    _id: mongoose.Schema.Types.ObjectId,
    id: {
      type: Number,
      unique: true,
      required: [true, 'The "id" parameter is required'],
    },
    name: {
      type: String,
      required: [true, 'The "name" parameter is required'],
    },
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

module.exports = mongoose.model('Store', storeSchema);
