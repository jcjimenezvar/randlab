# randlab

# RESTful API

This repository contains the implementation of any endpoints to resolve a randlab exam.

## Scripts

- `npm i` This command will be executed before start server, because this intall all dependencies required for the project
- `npm run dev` Start server in development mode with automatic refresh on changes
- `npm run start` Start server

# More documentation

To see more information about this implementation go to `http://localhost:8000/api-docs/`

```
